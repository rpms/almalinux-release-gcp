Summary: GCP specific packages from the AlmaLinux Cloud SIG
Name:    almalinux-release-gcp
Version: 9
Release: 1%{?dist}
License: GPLv2
URL:     https://wiki.almalinux.org/sigs/Cloud
Source0: almalinux-gcp.repo

Provides: almalinux-release-gcp = 9

%description
DNF configuration for GCP specific packages from the AlmaLinux Cloud SIG.

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/almalinux-gcp.repo

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/almalinux-gcp.repo

%changelog
* Tue May 02 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 9-1
- Initial version
